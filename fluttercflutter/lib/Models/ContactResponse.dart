// To parse this JSON data, do
//
//     final contactResponse = contactResponseFromJson(jsonString);

import 'dart:convert';

ContactResponse contactResponseFromJson(String str) => ContactResponse.fromJson(json.decode(str));

String contactResponseToJson(ContactResponse data) => json.encode(data.toJson());

class ContactResponse {
    String status;
    List<Object> objects;
    int totalObjects;

    ContactResponse({
        this.status,
        this.objects,
        this.totalObjects,
    });

    factory ContactResponse.fromJson(Map<String, dynamic> json) => ContactResponse(
        status: json["status"],
        objects: List<Object>.from(json["objects"].map((x) => Object.fromJson(x))),
        totalObjects: json["total_objects"],
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "objects": List<dynamic>.from(objects.map((x) => x.toJson())),
        "total_objects": totalObjects,
    };
}

class Object {
    bool deleted;
    int versionId;
    String telephoneNumber;
    DateTime createdAt;
    DateTime lastUpdatedAt;
    int id;
    DateTime birthdayDate;

    Object({
        this.deleted,
        this.versionId,
        this.telephoneNumber,
        this.createdAt,
        this.lastUpdatedAt,
        this.id,
        this.birthdayDate,
    });

    factory Object.fromJson(Map<String, dynamic> json) => Object(
        deleted: json["deleted"],
        versionId: json["version_id"],
        telephoneNumber: json["telephone_number"],
        createdAt: DateTime.parse(json["created_at"]),
        lastUpdatedAt: DateTime.parse(json["last_updated_at"]),
        id: json["id"],
        birthdayDate: DateTime.parse(json["birthday_date"]),
    );

    Map<String, dynamic> toJson() => {
        "deleted": deleted,
        "version_id": versionId,
        "telephone_number": telephoneNumber,
        "created_at": createdAt.toIso8601String(),
        "last_updated_at": lastUpdatedAt.toIso8601String(),
        "id": id,
        "birthday_date": "${birthdayDate.year.toString().padLeft(4, '0')}-${birthdayDate.month.toString().padLeft(2, '0')}-${birthdayDate.day.toString().padLeft(2, '0')}",
    };
}
