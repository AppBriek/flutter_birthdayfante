class FormattedContact {
  String name;
  String telephoneNumber;
  DateTime birthdayDate;
  String birthdayToString;
  int age;
  int daysUntilBirthday;  
}