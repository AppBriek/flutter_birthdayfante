import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttercflutter/Models/FormattedContact.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'AgeCalculation.dart';

class SyncCalendar {
  String calendarId = "";
  AgeCalculation ageCalculation;
  DeviceCalendarPlugin _deviceCalendarPlugin;
  var formattedContact = <FormattedContact>[];
  var events = <Event>[];

  SyncCalendar() {
    ageCalculation = new AgeCalculation();
    _deviceCalendarPlugin = DeviceCalendarPlugin();
  }

  getEvents() async {
    DateTime start = DateTime.now();
    DateTime end = DateTime.now().add(Duration(days: 365));
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    calendarId = prefs.getString("calendarId");
    var calendarEventResults = await _deviceCalendarPlugin.retrieveEvents(
        calendarId, RetrieveEventsParams(startDate: start, endDate: end));
    events = calendarEventResults.data;
  }

  addEvents(List<FormattedContact> contacts) async {
    formattedContact = contacts;
    for (var contact in formattedContact) {
      if (checkForDuplicates(contact.name) == false) {
        Event event = new Event(calendarId);
        event.title = contact.name + " Birthday";
        event.allDay = true;
        DateTime today = new DateTime.now();

        DateTime start = new DateTime(
            today.year, contact.birthdayDate.month, contact.birthdayDate.day);

        if (this.ageCalculation.checkIfNextYear(start) == true) {
          start = new DateTime(today.year + 1, contact.birthdayDate.month,
              contact.birthdayDate.day);
        }
        event.start = start;
        event.end = start;
        event.description = "It is the birthday of " + contact.name;
        var createEventResult =
            await _deviceCalendarPlugin.createOrUpdateEvent(event);
        if (createEventResult.isSuccess) {
          debugPrint("event is aangemaakt");
        }
      }
    }

    Fluttertoast.showToast(
        msg: "Calendar has been updated",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        fontSize: 16.0);
  }

  checkForDuplicates(String name) {
    bool exist = false;
    for (var event in events) {
      if (event.title == name + " Birthday") {
        debugPrint("Event exist");
        exist = true;
        return exist;
      }
    }
    return exist;
  }
}
