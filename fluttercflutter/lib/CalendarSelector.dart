import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:device_calendar/device_calendar.dart';
import 'package:fluttercflutter/Home.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CalendarSelector extends StatefulWidget {
  @override
  CalendarSelectorState createState() => CalendarSelectorState();
}

class CalendarSelectorState extends State<CalendarSelector> {
  DeviceCalendarPlugin _deviceCalendarPlugin;
  List<Calendar> _calendars;

  @override
  initState() {
    super.initState();
    getCalendars();
  }

  CalendarSelectorState() {
    _deviceCalendarPlugin = DeviceCalendarPlugin();
  }

  void saveChoice(String calendarId) async {
    debugPrint(calendarId);
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("calendarId", calendarId);
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => HomePage()));
  }

  void getCalendars() async {
    final calendarResult = await _deviceCalendarPlugin.retrieveCalendars();
    debugPrint(calendarResult?.data.first.name);
    setState(() {
      _calendars = calendarResult?.data;
    });
  }

  showAlertDialog(BuildContext context, String calendarId) {
    // set up the buttons
    Widget acceptButton = FlatButton(
      child: Text("Select"),
      onPressed: () {
        saveChoice(calendarId);
        Navigator.of(context).pop();
      },
    );

    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Select this calendar?"),
      content: Text(
          "Do you want to select this calendar to sync your contact birthdays?"),
      actions: [
        acceptButton,
        cancelButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(36, 23, 66, 1),
      appBar: AppBar(
        title: Text('Select calendar'),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(58, 39, 103, 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(25),
          ),
        ),
      ),
      body: Container(
        child: ListView.separated(
          padding: const EdgeInsets.all(8),
          itemCount: _calendars?.length ?? 0,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 8),
              height: 50,
              decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.circular(25)),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: () => this
                          .showAlertDialog(context, this._calendars[index].id),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              _calendars[index].name,
                              style: TextStyle(
                                color: Color.fromRGBO(36, 23, 66, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                              ),
                            ),
                          ]),
                    ),
                  ]),
            );
          },
          separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
        ),
      ),
    );
  }
}
