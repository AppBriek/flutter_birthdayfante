import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fluttercflutter/testAnimation.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:fluttercflutter/Models/FormattedContact.dart';
import 'package:fluttercflutter/Models/ContactResponse.dart';
import 'CalendarSelector.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:image_picker/image_picker.dart';
import 'package:fluttercflutter/SyncCalendar.dart';
import 'AgeCalculation.dart';

class HomePage extends StatefulWidget {
  @override
  HomepageState createState() => HomepageState();
}

class HomepageState extends State<HomePage> {
  List<Contact> contacts;
  var formattedContact = <FormattedContact>[];
  AgeCalculation ageCalculation;  
  ContactResponse contactResponse;
  File _image;
  String icon = "Assets/4year.png";

  HomepageState() {
    ageCalculation = new AgeCalculation();
    setProfileIcon();
    getContacts();
  }

  askPermissionContacts() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler()
            .requestPermissions([PermissionGroup.contacts]);
  }

  askPermissionCalendar() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.calendar);
    if (permission.value.toString() == "granted") {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => CalendarSelector()));
    } else {
      Map<PermissionGroup, PermissionStatus> permissions =
          await PermissionHandler()
              .requestPermissions([PermissionGroup.calendar]);
    }
  }

  getContacts() async {
    await askPermissionContacts();
    Iterable<Contact> contactsIt = await ContactsService.getContacts();
    contacts = contactsIt.toList();
    postcontacts();
  }

  postcontacts() async {
    var contactsFormatted = List<String>();
    for (Contact item in contacts) {
      for (var number in item.phones) {
        contactsFormatted.add(number.value.toString());
      }
    }
    var user = {};
    user['telephone_numbers'] = contactsFormatted;
    String str = json.encode(user);
    json.encode(contactsFormatted);
    var url = 'http://192.168.100.136:5000/profile/check';
    var response = await http.post(url, body: str);
    print(response.body);
    if (response.statusCode == 200) {
      contactResponse = contactResponseFromJson(response.body);
      prePareContact();
    } else {}
  }

  void setProfileIcon() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String profileIcon;
    DateTime birthday = DateTime.parse(prefs.get("birthdayDate"));
    int age = this.ageCalculation.calcAge(birthday);
    if (age > 0 && age < 5) {
      profileIcon = 'Assets/4year.png';
    } else if (age > 5 && age < 13) {
      profileIcon = 'Assets/12year.png';
    } else if (age > 13 && age < 19) {
      profileIcon = 'Assets/18year.png';
    } else if (age > 19 && age < 51) {
      profileIcon = 'Assets/50year.png';
    } else {
      profileIcon = 'Assets/99year.png';
    }
    setState(() {
      icon = profileIcon;
    });
  }

  void addCalendars() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String code = prefs.getString("calendarId");
    if (code?.isEmpty ?? true) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => CalendarSelector()));
    } else {
      SyncCalendar syncCalendar = new SyncCalendar();
      await syncCalendar.getEvents();
      syncCalendar.addEvents(this.formattedContact);
    }
  }

  void test() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => TestAnimation()));
  }

  void prePareContact() async {
    var formattedContactTemp = <FormattedContact>[];
    for (Contact contact in contacts) {
      for (Object contactje in contactResponse.objects) {
        for (Item number in contact.phones) {
          if (number.value == contactje.telephoneNumber) {
            //hij bestaat
            FormattedContact formattedContact = new FormattedContact();
            formattedContact.birthdayDate = contactje.birthdayDate;
            formattedContact.birthdayToString =
                contactje.birthdayDate.year.toString() +
                    "-" +
                    contactje.birthdayDate.month.toString() +
                    "-" +
                    contactje.birthdayDate.day.toString();
            formattedContact.name = contact.displayName;
            formattedContact.telephoneNumber = contactje.telephoneNumber;
            formattedContact.age = this.ageCalculation.calcAge(contactje.birthdayDate);
            formattedContact.daysUntilBirthday =
                this.ageCalculation.calcDaysBetweenDates(contactje.birthdayDate);
            formattedContactTemp.add(formattedContact);
            break;
          }
        }
      }
    }
    formattedContactTemp
        .sort((a, b) => a.daysUntilBirthday.compareTo(b.daysUntilBirthday));
    setState(() {
      formattedContact = formattedContactTemp;
    });
  }

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget remindButton = FlatButton(
      child: Text("Gallery"),
      onPressed: () {
        this.openGallery();
        Navigator.of(context).pop();
      },
    );
    Widget cancelButton = FlatButton(
      child: Text("Camera"),
      onPressed: () {
        this.openCamera();
        Navigator.of(context).pop();
      },
    );
    Widget launchButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("open Camera or Gallery"),
      content: Text(
          "Do you want to open the camera or gallery to upload a new profile picture?"),
      actions: [
        remindButton,
        cancelButton,
        launchButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  
  void openCamera() async {
    //await askPermissionCamera();
    debugPrint("opening camera");
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _image = image;
    });
  }

  void openGallery() async {
    //await askPermissionCamera();
    debugPrint("opening Gallery");
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(36, 23, 66, 1),
      appBar: AppBar(
        title: Text('BirthdayFante'),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.edit),
            onPressed: () {
              this.test();
            },
          ),
          new IconButton(
            icon: new Icon(Icons.refresh),
            onPressed: () {
              this.addCalendars();
            },
          )
        ],
        leading: new IconButton(
          icon: ClipOval(
            child: _image == null
                ? new Image.asset(
                    this.icon,
                    height: 60,
                    width: 60,
                    fit: BoxFit.cover,
                  )
                : Image.file(
                    _image,
                    height: 60,
                    width: 60,
                    fit: BoxFit.cover,
                  ),
          ),
          onPressed: () async {
            this.showAlertDialog(context);
          },
        ),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(58, 39, 103, 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(25),
          ),
        ),
      ),
      body: Container(
        child: ListView.separated(
          itemCount: formattedContact.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.only(left: 8, right: 8, top: 2, bottom: 8),
              height: 135,
              decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.circular(25)),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: Image.asset(
                        'Assets/4year.png',
                        height: 35,
                        width: 35,
                      ),
                    ),
                    Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${formattedContact[index].name.toString()}',
                            style: TextStyle(
                              color: Color.fromRGBO(36, 23, 66, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                          Text(
                            formattedContact[index].birthdayToString.toString(),
                            style:
                                TextStyle(color: Color.fromRGBO(36, 23, 66, 1)),
                          )
                        ]),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Days till birthday',
                          style:
                              TextStyle(color: Color.fromRGBO(91, 69, 141, 1)),
                        ),
                        Text(
                          formattedContact[index].daysUntilBirthday.toString(),
                          style: TextStyle(
                              color: Color.fromRGBO(91, 69, 141, 1),
                              fontSize: 25,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    )
                  ]),
            );
          },
          separatorBuilder: (BuildContext context, int index) =>
              const Divider(),
        ),
      ),
    );
  }
}
