import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttercflutter/Home.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'Home.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({this.title});

  final String title;
  @override
  RegisterPageState createState() => RegisterPageState();
}

class RegisterPageState extends State<RegisterPage> {
  DateTime selectedDate = DateTime.now();
  var txt = TextEditingController();
  String phonenumber = "";
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1900),
        lastDate: DateTime(2201));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        txt.text = selectedDate.day.toString() +
            "-" +
            selectedDate.month.toString() +
            "-" +
            selectedDate.year.toString();
      });
  }

  Future<HttpClientResponse> Registerhttp() async {
    Map<String, String> jsonMap = {
      'telephone_number': '32424234',
      'birthday_date': '2343'
    };
    String date = selectedDate.year.toString() +
        '-' +
        selectedDate.month.toString() +
        '-' +
        selectedDate.day.toString();
    var url = 'http://192.168.100.136:5000/profile';

    print(date);
    print(this.phonenumber);
    var response = await http.post(url,
        body: {'telephone_number': phonenumber, 'birthday_date': date});
    if (response.statusCode == 200) {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("birthdayDate", selectedDate.toString());

      Navigator.pushReplacement(
        context,
        MaterialPageRoute(builder: (context) => HomePage()),
      );
    } else {
      print('call could not be made');
    }
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(36, 23, 66, 1),
      appBar: AppBar(
        title: Text('Registeren'),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(58, 39, 103, 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(25),
          ),
        ),
      ),
      body: Center(
        child: Container(
          margin: EdgeInsets.all(18),
          height: 350,
          width: 600,
          decoration: new BoxDecoration(
            color: Colors.white,
            borderRadius: new BorderRadius.circular(25),
          ),
          child: Container(
            padding: EdgeInsets.only(top: 50),
            child: Column(
              children: [
                Text(
                  "BirthdayFante",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      color: Color.fromRGBO(19, 33, 98, 1)),
                ),
                myWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget myWidget() {
    return Container(
      margin: EdgeInsets.all(18),
      child: Column(
        children: [
          TextField(
            readOnly: true,
            onTap: () {
              _selectDate(context);
            },
            controller: txt,
            decoration: InputDecoration(
              hintText: "Birthday date",
              prefixText: "  |    ",
              border: new OutlineInputBorder(
                borderRadius: const BorderRadius.all(
                  const Radius.circular(30),
                ),
              ),
            ),
            keyboardType: null,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18),
            child: TextField(
              decoration: InputDecoration(
                hintText: 'PhoneNumber',
                prefixText: "  |    ",
                border: new OutlineInputBorder(
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(30),
                  ),
                ),
              ),
              keyboardType: TextInputType.number,
              onChanged: (text) {
                setState(() {
                  phonenumber = text;
                });
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 45),
            child: RaisedButton(
              padding:
                  EdgeInsets.only(left: 50, right: 50, top: 15, bottom: 15),
              textColor: Colors.white,
              color: Color.fromRGBO(123, 225, 237, 1),
              child: Text("Register",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
              onPressed: () => Registerhttp(),
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
