import 'package:flutter/cupertino.dart';

import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

class TestAnimation extends StatefulWidget {
  @override
  TestAnimationState createState() => TestAnimationState();
}

class TestAnimationState extends State<TestAnimation>
    with TickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 1), vsync: this);
    animation = Tween<double>(begin: 150, end: 200).animate(controller);

    controller.repeat();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(36, 23, 66, 1),
      appBar: AppBar(
        title: Text('Test pulse animation'),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(58, 39, 103, 1),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(25),
          ),
        ),
      ),
      body: AnimatedLogo(animation: animation),
    );
  }
}

class AnimatedLogo extends AnimatedWidget {
  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable as Animation<double>;
    return (Center(
      child: new Image.asset('Assets/4year.png',
          height: animation.value, width: animation.value),
    ));
  }
}
