class AgeCalculation {
  int calcDaysBetweenDates(DateTime birthDay) {
    DateTime today = DateTime.now();
    DateTime person = new DateTime(today.year, birthDay.month, birthDay.day);
    Duration difference = person.difference(today);
    int days = difference.inDays;
    if (days < 0) {
      person = new DateTime(today.year + 1, birthDay.month, birthDay.day);
      Duration extraYear = person.difference(today);
      return extraYear.inDays;
    }
    return days;
  }

  int calcAge(DateTime birthDay) {
    DateTime today = DateTime.now();
    int age = today.year - birthDay.year;
    return age;
  }

  bool checkIfNextYear(DateTime start) {
    DateTime today = new DateTime.now();
    DateTime endOfyear = new DateTime(today.year, 12, 31);
    Duration difference = endOfyear.difference(start);
    int days = difference.inDays;
    if (days < 0) {
      return true;
    }
    return false;
  }
}
